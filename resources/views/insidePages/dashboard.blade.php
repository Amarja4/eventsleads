@extends('app')
@section('content')

    {!! Form::open() !!}

    <h1>Dashboard</h1>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4><a href="{{ url('/auth/logout') }}">Log Out</a></h4>


    <hr>


    <div class="form-group">
        <a href="{{ url('lead/') }}" class = "btn btn-primary form-control">Leads</a>
    </div>
    <div class="form-group">
        <a href="{{ url('event/') }}" class = "btn btn-primary form-control">Events</a>
    </div>


    @include('errors.errors')
    {!! Form::close() !!}

@stop





