<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLeadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('lead', function (Blueprint $table) {
            $table->increments('lead_id');
            $table->integer('owner')->unsigned();
            $table->string('lead_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->text('area');
            $table->text('note');
            $table->rememberToken();
            $table->timestamps();
            $table->foreign('owner')->references('owner_id')->on('owner');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('lead');
    }
}
