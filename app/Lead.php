<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Lead extends Model
{

    protected $primaryKey = 'lead_id';
    protected $table = 'lead';

    protected $fillable = ['lead_name',
        'email',
        'phone',
        'area',
        'note',
//        'owner'
    ];

    //lead is created by owner.
    public function owner(){
        return $this->belongsTo('App\Owner');
    }
}
