<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Response;
//use App\Lead;
class LeadRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        if(Auth::check()){
            return true;
        }
        else{
            return false;
        }
    }
    public function forbiddenResponse()
    {
        return Response::make('Please Login',403);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {

        if ($this->method() == 'PATCH')
        {
            // Update operation, exclude the record with id from the validation:
            $email_rule =  'required|email|unique:lead,email,NULL,id,lead_id,'.Auth::user()->id;
        }
        else{
            $email_rule =  'required|email|unique:lead';
        }

        return [
            'lead_name' => 'required',
            'email' => $email_rule,
            'phone' => 'required|digits:10',
            'area' => 'required',
            'note' => 'required',
        ];
    }
}
