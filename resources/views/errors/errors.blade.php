@if($errors->any())
    <ul class="alert alert-danger">
        @foreach($errors->all() as $error)
{{--            <h5>Hiii{{$event->event_name}}</h5>--}}
            <li>{{ $error }}</li>
        @endforeach
    </ul>
@endif