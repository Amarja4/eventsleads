@extends('app')
@section('content')

{!! Form::open(array('url' => '/auth/login', 'class' => 'form-horizontal')) !!}

    <h1>Login</h1>
    <hr>

    <div class="form-group">
        {!! Form::label('email','Email') !!}
        {!! Form::text('email',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password','Password') !!}
        {!! Form::password('password',['class' => 'form-control'],null) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Log In',['class' => 'btn btn-primary']) !!}
    </div>
    <div class="form-group">
        <a href="{{ url('/auth/register') }}">Register</a>
    </div>
    {{--@if($errors->any())--}}
        {{--<ul class="alert alert-danger">--}}
            {{--@foreach($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
    {{--@endif--}}
@include('errors.errors')
    {!! Form::close() !!}

@stop