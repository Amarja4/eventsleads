<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    //
    protected $primaryKey = 'event_id';
    protected $table = 'event';

    protected $fillable = ['event_name',
        'event_location',
        'start_date',
        'end_date',
        'start_time',
        'end_time',
    ];

    //event is created by owner.
    public function owner(){
        return $this->belongsTo('App\Owner');
    }
}
