@extends('app')
@section('content')

    {!! Form::open(array('url' => '/auth/register', 'class' => 'form-horizontal')) !!}

    <h1>Register</h1>
    <hr>
    <div class="form-group">
        {!! Form::label('org_name','Organization Name') !!}
        {!! Form::text('org_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('owner_name','Owner Name') !!}
        {!! Form::text('owner_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Email ID') !!}
        {!! Form::text('email',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('phone','Phone') !!}
        {!! Form::text('phone',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('location','Location') !!}
        {!! Form::text('location',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password','Password') !!}
        {!! Form::password('password',['class' => 'form-control'],null) !!}
    </div>
    <div class="form-group">
        {!! Form::label('password_confirmation','Confirm Password') !!}
        {!! Form::password('password_confirmation',['class' => 'form-control'],null) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Register',['class' => 'btn btn-primary']) !!}
    </div>

    {!! Form::close() !!}
@include('errors.errors')
    {{--@if($errors->any())--}}
        {{--<ul class="alert alert-danger">--}}
            {{--@foreach($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
                {{--@endforeach--}}

        {{--</ul>--}}
    {{--@endif--}}
@stop
