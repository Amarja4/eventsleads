<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Authenticatable;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class Owner extends Model implements AuthenticatableContract,AuthorizableContract,CanResetPasswordContract
{
    //
    use Authenticatable, Authorizable, CanResetPassword;
    protected $primaryKey = 'owner_id';
    protected $table = 'owner';

    protected $fillable = ['org_name',
        'owner_name',
        'email',
        'phone',
        'password',
        'password_confirmation',
    ];

    protected $hidden = ['password', 'remember_token'];

    //owner has many leads
    public function leads(){
        return $this->hasMany('App\Lead', 'owner');
    }
    //owner has many events
    public function events(){
        return $this->hasMany('App\Event', 'owner');
    }
}
