@extends('app')
@section('content')

    {!! Form::open(array('action' => 'EventController@store','method' => 'post')) !!}

    <h1>Add Event</h1>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/dashboard') }}">Back To Dashboard</a></h4>

    <hr>

    <div class="form-group">
        {!! Form::label('event_name','Name :') !!}
        {!! Form::text('event_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('event_location','Location :') !!}
        {!! Form::text('event_location',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('start_date','Start Date :') !!}
{{--        {!! Form::input('date','start_date',date('d-m-Y'),['class' => 'form-control']) !!}--}}
        {!! Form::input('date','start_date',date('Y-m-d'),['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('end_date','End Date :') !!}
        {!! Form::input('date','end_date',date('Y-m-d'),['class' => 'form-control']) !!}
        {{--{!! Form::input('date','end_date',date('d-m-Y'),['class' => 'form-control']) !!}--}}

    </div>
    <div class="form-group">
        {!! Form::label('start_time','Start Time :') !!}
        {!! Form::input('time','start_time',date('H:i'),['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('end_time','End Time :') !!}
        {!! Form::input('time','end_time',date('H:i'),['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Add',['class' => 'btn btn-primary form-control']) !!}
    </div>

    @include('errors.errors')

    {!! Form::close() !!}

@stop

