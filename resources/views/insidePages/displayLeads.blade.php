@extends('app')
@section('content')
    <h2>Leads Information</h2>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4 class="pull-right"><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/dashboard') }}">Back To Dashboard</a></h4>
    <div class="form-group">
    <a href="{{ url('lead/create') }}" class = "btn btn-primary">Add Lead</a>
    </div>
    @foreach($leads as $lead)
        <div class="form-group clearfix">
            <div>
                <h4><b>Name: </b>{{$lead->lead_name}}</h4>
                <h4><b>Email: </b>{{$lead->email}}</h4>
                <h4><b>Phone: </b>{{$lead->phone}}</h4>
                <h4><b>Area: </b>{{$lead->area}}</h4>
                <h4><b>Note: </b>{{$lead->note}}</h4>
            </div>


            <div>
                <h4><a href="{{ url('lead/'.$lead->lead_id.'/edit') }}">Edit</a></h4>
            {!! Form::open(['method' => 'DELETE','route' => ['lead.destroy', $lead->lead_id]]) !!}
            {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
            {!! Form::close() !!}
            </div>
{{--            <h4><a href="{{ url('lead/'.$lead->lead_id) }}">Delete</a></h4>--}}

        </div>
            <hr>
    @endforeach
    @include('errors.errors')
@stop