@extends('app')
@section('content')
    <h2>Edit Event: {!! $event->event_name !!}</h2>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/event') }}">Back To Events</a></h4>
    {{--    {!! Form::open()!!}--}}
    {!! Form::model($event,['method' => 'PATCH','action'=>['EventController@update',$event->event_id]]) !!}


    <div class="form-group">
        {!! Form::label('event_name','Name') !!}
        {!! Form::text('event_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('event_location','Location') !!}
        {!! Form::text('event_location',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('start_date','Start Date') !!}
        {{--        {!! Form::input('date','start_date',date('d-m-Y'),['class' => 'form-control']) !!}--}}
        {!! Form::input('date','start_date',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('end_date','End Date') !!}
        {!! Form::input('date','end_date',null,['class' => 'form-control']) !!}

    </div>

    <div class="form-group">
        {!! Form::label('start_time','Start Time') !!}
        {!! Form::input('time','start_time',date('G:i', strtotime($event->start_time)),['class' => 'form-control']) !!}
{{--        {!! Form::input('time','start_time',null,['class' => 'form-control']) !!}--}}

    </div>
    <div class="form-group">
        {!! Form::label('end_time','End Time') !!}
        {!! Form::input('time','end_time',date('G:i', strtotime($event->end_time)),['class' => 'form-control']) !!}
{{--        {!! Form::input('time','end_time',null,['class' => 'form-control']) !!}--}}
    </div>
    <div class="form-group">
        {!! Form::submit('Update',['class' => 'btn btn-primary']) !!}
    </div>

    @include('errors.errors')

    {!! Form::close() !!}


@stop

