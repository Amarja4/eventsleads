<?php

namespace App\Http\Controllers;

use App\Lead;
use Request;
use Auth;
use App\Http\Requests;
use App\Http\Requests\LeadRequest;
use App\Http\Controllers\Controller;


class LeadController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
//        $leads = Lead::latest('updated_at')->get();
        $leads = Auth::user()->leads()->latest('updated_at')->get();
        return view('insidePages.displayLeads',compact('leads'));

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('insidePages.addLead');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(LeadRequest $request)
    {
        //
        $lead = new Lead($request->all());
        Auth::user()->leads()->save($lead);
//        Lead::create(Request::all());

        return redirect('/lead');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
//    public function show($id)
//    {
//        //
//        $lead = Lead::findOrFail('$id');
//        return view('insidePages.editLead',compact('lead'));
////        return redirect('/lead');
//
//    }

//    /**
//     * Show the form for editing the specified resource.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
    public function edit($id)
    {
        $lead = Lead::findOrFail($id);
        return view('insidePages.editLead',compact('lead'));
    }
//    /**
//     * Update the specified resource in storage.
//     *
//     * @param  \Illuminate\Http\Request  $request
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
    public function update(LeadRequest $request, $id)
    {
        //
        $lead = Lead::findOrFail($id);
        $lead->update($request->all());
        return redirect('lead');
    }

//    /**
//     * Remove the specified resource from storage.
//     *
//     * @param  int  $id
//     * @return \Illuminate\Http\Response
//     */
    public function destroy($id)
    {
        $lead = Lead::findOrFail($id);
        $lead->delete();
        return redirect('lead');
//        $lead->destroy($request->all());
    }

}
