<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOwnerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('owner', function (Blueprint $table) {
            $table->increments('owner_id');
            $table->string('org_name');
            $table->string('owner_name');
            $table->string('email')->unique();
            $table->string('phone');
            $table->string('password', 60);
            $table->string('password_confirmation');
            $table->rememberToken();
            $table->timestamps();


//            $table->timestamp('regi_at');
//            $table->bigInteger('auto_id')->unsigned();
//            $table->unique('auto_id');
//            $table->primary('auto_id');
//            $table->double('latitude');
//            $table->double('longitude');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('owner');
    }
}
