@extends('app')
@section('content')
    <h2>Events Information</h2>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4 class="pull-right"><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/dashboard') }}">Back To Dashboard</a></h4>
    <div class="form-group">
    <a href="{{ url('event/create') }}" class = "btn btn-primary">Add Event</a>
    </div>
    @foreach($events as $event)
        <div class="form-group clearfix">
            <div>
            <h4><b>Name: </b>{{$event->event_name}}</h4>
            <h4><b>Location: </b>{{$event->event_location}}</h4>
            <h4><b>Start Date: </b>{{$event->start_date}}</h4>
            <h4><b>End Date: </b>{{$event->end_date}}</h4>
            <h4><b>Start Time: </b>{{$event->start_time}}</h4>
            <h4><b>End Time: </b>{{$event->end_time}}</h4>
            </div>
            <div>
                <h4><a href="{{ url('event/'.$event->event_id.'/edit') }}">Edit</a></h4>
                {!! Form::open(['method' => 'DELETE','route' => ['event.destroy', $event->event_id]]) !!}
                {!! Form::submit('Delete', ['class' => 'btn btn-danger']) !!}
                {!! Form::close() !!}
            </div>
            {{--            <h4><a href="{{ url('lead/'.$lead->lead_id) }}">Delete</a></h4>--}}
        </div>
            <hr>

    @endforeach
    @include('errors.errors')
@stop