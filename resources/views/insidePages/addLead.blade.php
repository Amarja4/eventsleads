@extends('app')
@section('content')

    {!! Form::open(array('action' => 'LeadController@store','method' => 'post')) !!}

    <h1>Add Lead</h1>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/dashboard') }}">Back To Dashboard</a></h4>

    <hr>

    <div class="form-group">
        {!! Form::label('lead_name','Name :') !!}
        {!! Form::text('lead_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Email :') !!}
        {!! Form::text('email',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('phone','Phone :') !!}
        {!! Form::text('phone',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('area','Area :') !!}
        {!! Form::text('area',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('note','Note :') !!}
        {!! Form::textarea('note',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Add',['class' => 'btn btn-primary form-control']) !!}
    </div>

    {{--@if($errors->any())--}}
        {{--<ul class="alert alert-danger">--}}
            {{--@foreach($errors->all() as $error)--}}
                {{--<li>{{ $error }}</li>--}}
            {{--@endforeach--}}
        {{--</ul>--}}
    {{--@endif--}}
    @include('errors.errors')

    {!! Form::close() !!}

@stop

