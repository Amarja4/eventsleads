<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Validator;
use Input;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
        Validator::extend('equal_after', function($attribute, $value, $parameters) {
            return strtotime(Input::get($parameters[0])) <= strtotime($value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
