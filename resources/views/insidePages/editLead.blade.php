@extends('app')
@section('content')
    <h2>Edit Lead: {!! $lead->lead_name !!}</h2>
    <h2>Welcome {{Auth::user()->owner_name}}!</h2>
    <h4><a href="{{ url('/auth/logout') }}">Log Out</a></h4>
    <h4><a href="{{ url('/lead') }}">Back To Leads</a></h4>
{{--    {!! Form::open()!!}--}}
        {!! Form::model($lead,['method' => 'PATCH','action'=>['LeadController@update',$lead->lead_id]]) !!}

    <div class="form-group">
        {!! Form::label('lead_name','Name :') !!}
        {!! Form::text('lead_name',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('email','Email :') !!}
        {!! Form::text('email',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('phone','Phone :') !!}
        {!! Form::text('phone',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('area','Area :') !!}
        {!! Form::text('area',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::label('note','Note :') !!}
        {!! Form::textarea('note',null,['class' => 'form-control']) !!}
    </div>
    <div class="form-group">
        {!! Form::submit('Update',['class' => 'btn btn-primary form-control']) !!}
    </div>

@include('errors.errors')

    {!! Form::close() !!}
@stop
