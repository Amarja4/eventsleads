<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Auth;
use Response;

class EventRequest extends Request
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */

    public function authorize()
    {
        if(Auth::check()){
            return true;
        }
        else{
            return false;
        }
    }
    public function forbiddenResponse()
    {
        return Response::make('Please Login',403);
    }
    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules()
    {

        return [
            'event_name' => 'required',
            'event_location' => 'required',
//            'start_date' => 'required|date_format:Y/m/d|after:today',
            'start_date' => 'required|date|after:today',
            'end_date' => 'required|date|equal_after:start_date',
            'start_time' => 'required|date_format:H:i',
            'end_time' => 'required|date_format:H:i|after:start_time',


        ];

    }
}
